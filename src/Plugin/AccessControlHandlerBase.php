<?php

namespace Drupal\pach\Plugin;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Base class for AccessControlHandler plugins.
 *
 * @see \Drupal\pach\Annotation\AccessControlHandler
 * @see \Drupal\pach\AccessControlHandlerPluginManager
 * @see \Drupal\pach\AccessControlHandlerInterface
 * @see plugin_api
 */
abstract class AccessControlHandlerBase extends PluginBase implements AccessControlHandlerInterface {

  /**
   * The plugin settings.
   *
   * @var array<string, mixed>
   */
  protected $settings;

  /**
   * Name of the entity type the handler controls access for.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): AccessControlHandlerInterface {
    if (isset($configuration['type'])) {
      $this->type = $configuration['type'];
    }
    if (isset($configuration['settings'])) {
      $this->settings = (array) $configuration['settings'];
    }
    return $this;
  }

  /**
   * Gets this plugin's configuration.
   *
   * @return array<string, mixed>
   *   An array of this plugin's configuration.
   */
  public function getConfiguration(): array {
    return [
      'id' => $this->getPluginId(),
      'type' => $this->type,
      'settings' => $this->settings,
    ];
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array<string, mixed>
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    /** @var array<string, mixed> $plugin_definition */
    $plugin_definition = $this->pluginDefinition;
    return [
      'type' => $plugin_definition['type'],
      'settings' => $plugin_definition['settings'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType(): string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccessResultInterface &$access, EntityInterface $entity, string $operation, AccountInterface $account = NULL): void {
    // Override this method to alter the access result.
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess(AccessResultInterface &$access, $entity_bundle = NULL, AccountInterface $account = NULL, array $context = []): void {
    // Override this method to alter the access result.
  }

  /**
   * {@inheritdoc}
   */
  public function fieldAccess(AccessResultInterface &$access, string $operation, FieldDefinitionInterface $field_definition, AccountInterface $account = NULL, FieldItemListInterface $items = NULL): void {
    // Override this method to alter the access result.
  }

}
