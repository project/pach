<?php

namespace Drupal\pach\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an interface for pluggable access control handlers.
 */
interface PluggableAccessControlHandlerInterface {

  /**
   * Instantiates a new instance of this pluggable access control handler.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return static
   *   A new instance of the pluggable access control handler.
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager): static;

}
