<?php

namespace Drupal\pach\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler as CoreEntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\pach\AccessControlHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a pluggable implementation for entity access control handler.
 */
class EntityAccessControlHandler extends CoreEntityAccessControlHandler implements PluggableAccessControlHandlerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The original access handler instance.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $accessHandlerOriginal;

  /**
   * The access control handler plugin manager.
   *
   * @var \Drupal\pach\AccessControlHandlerManager
   */
  protected $pluginManager;

  /**
   * List of applicable access control handlers.
   *
   * @var \Drupal\pach\Plugin\AccessControlHandlerInterface[]
   */
  protected $handlers = [];

  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * Allows to grant access to just the labels.
   *
   * @var bool
   */
  protected $viewLabelOperation = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager, AccessControlHandlerManager $plugin_manager, SerializationInterface $serializer) {
    parent::__construct($entity_type);
    $this->entityTypeManager = $entity_type_manager;
    $this->serializer = $serializer;

    if (!$this->entityType->hasHandlerClass('_access')) {
      throw new InvalidPluginDefinitionException($this->entityTypeId, sprintf('The "%s" entity type did not specify a %s handler.', $this->entityTypeId, '_access'));
    }
    /** @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface $original_handler */
    $original_handler = $this->entityTypeManager->getHandler($this->entityTypeId, '_access');
    $this->accessHandlerOriginal = $original_handler;
    $this->pluginManager = $plugin_manager;
    $this->handlers = $this->pluginManager->getHandlers($this->entityTypeId);

    // By default, the "view label" operation falls back to "view". Some access
    // control handlers (e.g. \Drupal\user\UserAccessControlHandler) set this to
    // <code>TRUE</code> so we need to set the property based on the parent
    // class property. Since the property is protected we need to get our hands
    // dirty and use reflection.
    $reflection_class = new \ReflectionClass($this->accessHandlerOriginal);
    try {
      $property = $reflection_class->getProperty('viewLabelOperation');
      $this->viewLabelOperation = (bool) $property->getValue($this->accessHandlerOriginal);
    }
    catch (\ReflectionException $exc) {
      $this->viewLabelOperation = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager): static {
    // @phpstan-ignore-next-line
    return new static(
      $entity_type,
      $entity_type_manager,
      $container->get('plugin.manager.pach'),
      $container->get('serialization.phpserialize')
    );
  }

  /**
   * Call functions of original access control handlers.
   *
   * @param string $name
   *   Name of function.
   * @param array $arguments
   *   Arguments for original function.
   *
   * @return mixed
   *   Return value of original function.
   *
   * @throws \Exception
   *   A generic exception is thrown if the called method does not exist in
   *   original entity access control handler.
   */
  public function __call(string $name, array $arguments) {
    if (!method_exists($this->accessHandlerOriginal, $name)) {
      throw new \Exception(sprintf('Method "%s" does not exists for class "%s".', $name, get_class($this->accessHandlerOriginal)));
    }
    /** @var callable $callback */
    $callback = [$this->accessHandlerOriginal, $name];
    return call_user_func_array($callback, $arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $langcode = $entity->language()->getId();
    $account_loaded = $this->prepareUser($account);

    if ($operation === 'view label' && $this->viewLabelOperation == FALSE) {
      $operation = 'view';
    }

    // Build cache ID.
    $cid = $entity->uuid() ?: $entity->getEntityTypeId() . ':' . $entity->id();

    // If the entity is revisionable, then append the revision ID to allow
    // individual revisions to have specific access control and be cached
    // separately.
    if ($entity instanceof RevisionableInterface) {
      /** @var \Drupal\Core\Entity\RevisionableInterface $entity */
      $cid .= ':' . $entity->getRevisionId();
      // It is not possible to delete or revert the default revision.
      if ($entity->isDefaultRevision() && ($operation === 'revert' || $operation === 'delete revision')) {
        return $return_as_object ? AccessResult::forbidden() : FALSE;
      }
    }

    // Append custom string to cache ID to avoid side effects with core caching.
    $cid .= ':pach';

    if (($cached_return = $this->getCache($cid, $operation, $langcode, $account_loaded)) !== NULL) {
      // Cache hit, no work necessary.
      return $return_as_object ? $cached_return : $cached_return->isAllowed();
    }

    if ($this->originalHandlerHasMethod('access')) {
      // Use access() method of original access control handler. This may be
      // \Drupal\Core\Entity\EntityAccessControlHandler::access() in case the
      // original handler does not override the method but we do not know this.
      $return = $this->accessHandlerOriginal->access($entity, $operation, $account_loaded, TRUE);
    }
    else {
      // Call \Drupal\Core\Entity\EntityAccessControlHandler::access().
      $return = parent::access($entity, $operation, $account_loaded, TRUE);
    }

    // Let all applicable handlers alter the access result.
    foreach ($this->handlers as $handler) {
      if ($handler->applies($entity, $operation, $account_loaded) && method_exists($handler, 'access')) {
        $handler->access($return, $entity, $operation, $account_loaded);
      }
    }

    $result = $this->setCache($return, $cid, $operation, $langcode, $account_loaded);
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($this->originalHandlerHasMethod('checkAccess')) {
      $arguments = [
        $entity,
        $operation,
        $account,
      ];
      /** @var \Drupal\Core\Access\AccessResultInterface $return */
      $return = $this->originalHandlerCall('checkAccess', $arguments);
      return $return;
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function createAccess($entity_bundle = NULL, AccountInterface $account = NULL, array $context = [], $return_as_object = FALSE) {
    $account_loaded = $this->prepareUser($account);
    $context += [
      'entity_type_id' => $this->entityTypeId,
      'langcode' => LanguageInterface::LANGCODE_DEFAULT,
    ];

    // Prepare context array for serialization.
    ksort($context);

    $cid = $entity_bundle ? 'create:' . $entity_bundle : 'create:pach';
    if (($access = $this->getCache($cid, 'create', $context['langcode'], $account_loaded)) !== NULL) {
      // Cache hit, no work necessary.
      return $return_as_object ? $access : $access->isAllowed();
    }

    if ($this->originalHandlerHasMethod('createAccess')) {
      // Use createAccess() method of original access control handler. This may
      // be \Drupal\Core\Entity\EntityAccessControlHandler::createAccess() in
      // case the original handler does not override the method but we do not
      // know this.
      $return = $this->accessHandlerOriginal->createAccess($entity_bundle, $account_loaded, $context, TRUE);
    }
    else {
      // Call \Drupal\Core\Entity\EntityAccessControlHandler::createAccess().
      $return = parent::createAccess($entity_bundle, $account_loaded, $context, TRUE);
    }

    // Process plugins.
    foreach ($this->handlers as $handler) {
      if (!method_exists($handler, 'createAccess')) {
        continue;
      }
      $handler->createAccess($return, $entity_bundle, $account_loaded, $context);
    }

    $result = $this->setCache($return, $cid, 'create', $context['langcode'], $account_loaded);
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($this->originalHandlerHasMethod('checkCreateAccess')) {
      $arguments = [
        $account,
        $context,
        $entity_bundle,
      ];
      /** @var \Drupal\Core\Access\AccessResultInterface $return */
      $return = $this->originalHandlerCall('checkCreateAccess', $arguments);
      return $return;
    }

    // No opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account = NULL, FieldItemListInterface $items = NULL, $return_as_object = FALSE) {
    $account_loaded = $this->prepareUser($account);

    if ($this->originalHandlerHasMethod('createAccess')) {
      // Use fieldAccess() method of original access control handler. This may
      // be \Drupal\Core\Entity\EntityAccessControlHandler::fieldAccess() in
      // case the original handler does not override the method but we do not
      // know this.
      $return = $this->accessHandlerOriginal->fieldAccess($operation, $field_definition, $account, $items, TRUE);
    }
    else {
      // Call \Drupal\Core\Entity\EntityAccessControlHandler::fieldAccess().
      $return = parent::fieldAccess($operation, $field_definition, $account_loaded, $items, TRUE);
    }

    // Process plugins.
    foreach ($this->handlers as $handler) {
      if (!method_exists($handler, 'fieldAccess')) {
        continue;
      }
      $handler->fieldAccess($return, $operation, $field_definition, $account_loaded, $items);
    }

    return $return_as_object ? $return : $return->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    if ($this->originalHandlerHasMethod('checkFieldAccess')) {
      $arguments = [
        $operation,
        $field_definition,
        $account,
        $items,
      ];
      /** @var \Drupal\Core\Access\AccessResultInterface $return */
      $return = $this->originalHandlerCall('checkFieldAccess', $arguments);
      return $return;
    }

    // No opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache(): void {
    $this->accessHandlerOriginal->resetCache();
    $this->accessCache = [];
  }

  /**
   * Check if the original access control handler has a specific method.
   *
   * @param string $method
   *   Name of method to find.
   *
   * @return bool
   *   <code>TRUE</code> if the method exists, otherwise <code>FALSE</code>.
   */
  protected function originalHandlerHasMethod(string $method): bool {
    $reflection_class = new \ReflectionClass($this->accessHandlerOriginal);
    return $reflection_class->hasMethod($method);
  }

  /**
   * Call a method in the original access control handler.
   *
   * @param string $method
   *   Name of method to call.
   * @param array $arguments
   *   List of arguments to pass to the method.
   *
   * @return mixed
   *   Result of the method call.
   */
  protected function originalHandlerCall(string $method, array $arguments) {
    if ($this->originalHandlerHasMethod($method)) {
      $reflection_class = new \ReflectionClass($this->accessHandlerOriginal);
      $reflection_method = $reflection_class->getMethod($method);
      return $reflection_method->invokeArgs($this->accessHandlerOriginal, $arguments);
    }
  }

}
