<?php

namespace Drupal\pach\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManager as CoreEntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides core EntityTypeManager to allow pluggable access control handlers.
 *
 * @see \Drupal\Core\Entity\EntityTypeManager
 * @phpstan-ignore-next-line
 */
class EntityTypeManager extends CoreEntityTypeManager implements EntityTypeManagerInterface {

  /**
   * The decorated entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $coreEntityTypeManager;

  /**
   * Constructs a new Entity plugin manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The decorated entity type manager.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to use.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository
   *   The entity last installed schema repository.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface|null $container
   *   The service container.
   *
   * @phpstan-ignore-next-line
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, \Traversable $namespaces, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache, TranslationInterface $string_translation, ClassResolverInterface $class_resolver, EntityLastInstalledSchemaRepositoryInterface $entity_last_installed_schema_repository, protected ?ContainerInterface $container = NULL) {
    $this->coreEntityTypeManager = $entity_type_manager;
    parent::__construct($namespaces, $module_handler, $cache, $string_translation, $class_resolver, $entity_last_installed_schema_repository, $container);
  }

  /**
   * {@inheritdoc}
   */
  public function getHandler($entity_type, $handler_type) {
    /** @var \Drupal\Core\Entity\EntityTypeInterface $handler_orig */
    $handler_orig = parent::getHandler($entity_type, $handler_type);
    if ('access' !== $handler_type) {
      // Do not alter other handler types than "access".
      return $handler_orig;
    }
    if ($handler_orig instanceof PluggableAccessControlHandlerInterface) {
      // Handler is already altered.
      return $handler_orig;
    }

    // Replace original access control handler.
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    $definition = $this->getDefinition($entity_type);
    $_class = $definition->getHandlerClass($handler_type);
    if (!$_class) {
      throw new InvalidPluginDefinitionException($entity_type, sprintf('The "%s" entity type did not specify a %s handler.', $entity_type, $handler_type));
    }
    $definition->setHandlerClass('_access', $_class);
    $class = 'Drupal\pach\Entity\EntityAccessControlHandler';
    $this->handlers[$handler_type][$entity_type] = $this->createHandlerInstance($class, $definition);
    return $this->handlers[$handler_type][$entity_type];
  }

  /**
   * {@inheritdoc}
   */
  public function createHandlerInstance($class, EntityTypeInterface $definition = NULL) {
    // @phpstan-ignore-next-line
    if (is_subclass_of($class, 'Drupal\Core\Entity\EntityHandlerInterface')) {
      // @phpstan-ignore-next-line
      $handler = $class::createInstance($this->container, $definition);
    }
    // @phpstan-ignore-next-line
    elseif (is_subclass_of($class, 'Drupal\pach\Entity\PluggableAccessControlHandlerInterface')) {
      // @phpstan-ignore-next-line
      $handler = $class::createInstance($this->container, $definition, $this);
    }
    else {
      $handler = new $class($definition);
    }
    if (method_exists($handler, 'setModuleHandler')) {
      $handler->setModuleHandler($this->moduleHandler);
    }
    if (method_exists($handler, 'setStringTranslation')) {
      $handler->setStringTranslation($this->stringTranslation);
    }

    return $handler;
  }

}
