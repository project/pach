<?php

namespace Drupal\pach\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * The AccessControlHandler attribute.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class AccessControlHandler extends Plugin {

  /**
   * Constructs an AccessControlHandler attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param string $type
   *   ID of the entity type the handler controls access for.
   * @param int $weight
   *   (optional) The plugin weight. Defaults to 0.
   */
  public function __construct(
    public readonly string $id,
    public readonly string $type,
    public readonly int $weight = 0,
  ) {}

}
