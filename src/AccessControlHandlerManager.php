<?php

namespace Drupal\pach;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\pach\Attribute\AccessControlHandler;

/**
 * Manages access control handlers.
 *
 * @see hook_pach_handler_info_alter()
 * @see \Drupal\pach\Annotation\AccessControlHandler
 * @see \Drupal\pach\Plugin\AccessControlHandlerInterface
 * @see \Drupal\pach\Plugin\AccessControlHandlerBase
 * @see plugin_api
 */
class AccessControlHandlerManager extends DefaultPluginManager {

  /**
   * Constructs a AccessControlHandlerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/pach', $namespaces, $module_handler, 'Drupal\pach\Plugin\AccessControlHandlerInterface', AccessControlHandler::class, 'Drupal\pach\Annotation\AccessControlHandler');

    $this->alterInfo('pach_handler_info');
    $this->setCacheBackend($cache_backend, 'pach_handlers');
  }

  /**
   * Get a list of all registered handler instances sorted by weight.
   *
   * @param string $entity_type
   *   Limit handlers to the given entity type.
   *
   * @return \Drupal\pach\Plugin\AccessControlHandlerInterface[]
   *   List of processor plugin instances, optionally limited to an entity type.
   */
  public function getHandlers($entity_type) {
    /** @var array<string, mixed> $instances */
    $instances = &drupal_static(__FUNCTION__, []);
    if (!empty($instances[$entity_type])) {
      /** @var \Drupal\pach\Plugin\AccessControlHandlerInterface[] $instance_handlers */
      $instance_handlers = $instances[$entity_type];
      return $instance_handlers;
    }

    $instances[$entity_type] = [];
    /** @var \Drupal\pach\Plugin\AccessControlHandlerInterface[] $handlers */
    $handlers = $this->limitHandlers($this->getDefinitions(), $entity_type);
    /** @var callable $sort_callback */
    $sort_callback = [
      'Drupal\Component\Utility\SortArray',
      'sortByWeightElement',
    ];
    uasort($handlers, $sort_callback);
    foreach ($handlers as $plugin_id => $handler) {
      // Execute the processor plugin.
      // @phpstan-ignore-next-line
      $instances[$entity_type][$plugin_id] = $this->createInstance($plugin_id, $handler);
    }

    /** @var \Drupal\pach\Plugin\AccessControlHandlerInterface[] $instance_handlers */
    $instance_handlers = $instances[$entity_type];
    return $instance_handlers;
  }

  /**
   * Reduce a list of access control handlers to a single entity type.
   *
   * @param array $handlers
   *   List of access control handlers.
   * @param string $entity_type
   *   Name of entity type.
   *
   * @return \Drupal\pach\Plugin\AccessControlHandlerInterface[]
   *   List of access control handlers for the given entity type.
   */
  protected function limitHandlers(array $handlers, $entity_type) {
    return array_filter($handlers, function ($handler) use ($entity_type) {
      /** @var \Drupal\pach\Plugin\AccessControlHandlerInterface|array<string, mixed> $handler */
      if (is_array($handler)) {
        return $entity_type === $handler['type'];
      }
      return $entity_type === $handler->getEntityType();
    });
  }

}
