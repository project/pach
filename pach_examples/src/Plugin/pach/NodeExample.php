<?php

namespace Drupal\pach_examples\Plugin\pach;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\pach\Attribute\AccessControlHandler;
use Drupal\pach\Plugin\AccessControlHandlerBase;

/**
 * Example access control handler plugin for nodes.
 */
#[AccessControlHandler(
  id: 'node_example',
  type: 'node',
  weight: -10
)]
class NodeExample extends AccessControlHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function applies(EntityInterface $entity, string $operation, AccountInterface $account = NULL): bool {
    /** @var \Drupal\node\NodeInterface $entity */
    // Applies to all nodes of type "article".
    return ($entity->bundle() === 'article');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccessResultInterface &$access, EntityInterface $entity, string $operation, AccountInterface $account = NULL): void {
    /** @var \Drupal\node\NodeInterface $entity */
    /** @var string $label */
    $label = $entity->label();
    if (($operation === 'update') && (stripos($label, 'test') !== FALSE)) {
      // Generally deny edit access to all nodes having the string "test" in its
      // title.
      $access = $access->andIf(AccessResult::forbidden());
    }
  }

}
