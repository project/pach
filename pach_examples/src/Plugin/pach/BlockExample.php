<?php

namespace Drupal\pach_examples\Plugin\pach;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\pach\Attribute\AccessControlHandler;
use Drupal\pach\Plugin\AccessControlHandlerBase;

/**
 * Example access control handler plugin for blocks.
 */
#[AccessControlHandler(
  id: 'block_example',
  type: 'block',
  weight: -10
)]
class BlockExample extends AccessControlHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function applies(EntityInterface $entity, string $operation, AccountInterface $account = NULL): bool {
    // Applies to all blocks in region "header".
    /** @var \Drupal\block\BlockInterface $entity */
    return ($entity->getRegion() === 'header');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccessResultInterface &$access, EntityInterface $entity, string $operation, AccountInterface $account = NULL): void {
    /** @var \Drupal\block\BlockInterface $entity */
    if (($entity->id() === 'olivero_site_branding') && (date('w') === '2')) {
      // Hide the branding block in olivero on tuesdays.
      $access = $access->andIf(AccessResult::forbidden());
    }
  }

}
