[//]: # cspell:ignore Borchert stborchert undpaul

# pluggable Access Control Handler

### Introduction

The "pluggable Access Control Handler" ("pACH") module extends the default
entity access control handlers with an extensible plugin system.

This allows modules to maintain access control for entity types using plugins
without overriding the main handler.
It is not limited to nodes or terms or other content entities but influences
**all** entities having an access control handler defined in their definitions.


### Requirements

_none_


### Installation

Install as usual, see [Installing Drupal 8 Modules][1] for further information.


### Maintainers

* Stefan Borchert (stborchert) - http://drupal.org/user/36942

This project has been sponsored by:

  * _undpaul_

    Drupal experts providing professional Drupal development services.
    Visit [http://www.undpaul.de][2] or [https://www.drupal.org/undpaul][3]
    for more information.


[1]: https://www.drupal.org/documentation/install/modules-themes/modules-8
[2]: https:///www.undpaul.de
[3]: https://www.drupal.org/undpaul
