<?php

namespace Drupal\Tests\pach\Kernel\Entity;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\entity_test\Entity\EntityTestLabel;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\user\Entity\User;

/**
 * Tests the pluggable entity access control handler.
 *
 * @coversDefaultClass \Drupal\pach\Entity\EntityAccessControlHandler
 * @group Entity
 * @group pACH
 */
class EntityAccessControlHandlerTest extends EntityKernelTestBase {

  /**
   * List of modules required by the test.
   *
   * @var array<string>
   */
  protected static $modules = ['pach', 'pach_test'];

  /**
   * Asserts entity access correctly grants or denies access.
   *
   * @internal
   */
  public function assertEntityAccess(array $ops, AccessibleInterface $object, AccountInterface $account = NULL): void {
    foreach ($ops as $op => $expected) {
      $message = new FormattableMarkup("Entity access returns @expected with operation '@op'.", [
        '@expected' => !isset($expected) ? 'null' : ($expected ? 'true' : 'false'),
        '@op' => $op,
      ]);

      $this->assertEquals($object->access($op, $account), $expected, $message);
    }
  }

  /**
   * Ensures user labels are accessible for everyone.
   */
  public function testUserLabelAccess(): void {
    // Set up a non-admin user.
    \Drupal::currentUser()->setAccount($this->createUser([], NULL, FALSE, ['uid' => 2]));

    // Create an anonymous user.
    $anonymous_user = User::getAnonymousUser();
    // Setup a default user.
    $user = $this->createUser();
    // Setup an user having "*@example.org" as email.
    $mail = $this->randomMachineName() . '@example.org';
    $user_org = $this->createUser([], NULL, FALSE, ['mail' => $mail]);

    // The current user is allowed to view the anonymous user label.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => TRUE,
    ], $anonymous_user);

    // The current user is allowed to view authenticated user labels.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => TRUE,
    ], $user);

    // Switch to an anonymous user account.
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo(new AnonymousUserSession());

    // The anonymous user is allowed to view the anonymous user label.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => TRUE,
    ], $anonymous_user);

    // The anonymous user is allowed to view authenticated user labels.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => TRUE,
    ], $user);

    // Restore user account.
    $account_switcher->switchBack();

    // Switch to second authenticated user.
    $account_switcher->switchTo($user_org);

    // The user is allowed to view the anonymous user label.
    // @see \Drupal\pach_test\Plugin\pach\TestUser::applies()
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => TRUE,
    ], $anonymous_user);

    // The user is not allowed to view authenticated user labels.
    // @see \Drupal\pach_test\Plugin\pach\TestUser::access()
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => FALSE,
    ], $user);

    // Restore user account.
    $account_switcher->switchBack();
  }

  /**
   * Ensures entity access is properly working.
   */
  public function testEntityAccess(): void {
    // Set up a non-admin user that is allowed to view test entities.
    \Drupal::currentUser()->setAccount($this->createUser(['view test entity'], NULL, FALSE, ['uid' => 2]));

    // Use the 'entity_test_label' entity type in order to test the 'view label'
    // access operation.
    $entity = EntityTestLabel::create([
      'name' => 'test',
    ]);

    // The current user is allowed to view entities.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => TRUE,
      'view label' => TRUE,
    ], $entity);

    // The custom user is not allowed to perform any operation on test entities,
    // except for viewing their label.
    $custom_user = $this->createUser();
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => TRUE,
    ], $entity, $custom_user);

    $entity_no_view = EntityTestLabel::create([
      'name' => 'test no view',
    ]);

    // The current user is allowed to view entity labels only for entities with
    // "no view" in title.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => TRUE,
      'view label' => FALSE,
    ], $entity_no_view);

    // The custom user is not allowed to perform any operation on test entities,
    // except for viewing their label.
    $this->assertEntityAccess([
      'create' => FALSE,
      'update' => FALSE,
      'delete' => FALSE,
      'view' => FALSE,
      'view label' => FALSE,
    ], $entity_no_view, $custom_user);
  }

}
