<?php

namespace Drupal\Tests\pach\Kernel\Core\block_content;

use Drupal\Tests\block_content\Kernel\BlockContentAccessHandlerTest as CoreBlockContentAccessHandlerTest;

/**
 * Tests the block content entity access handler.
 *
 * @coversDefaultClass \Drupal\block_content\BlockContentAccessControlHandler
 *
 * @group block_content
 * @group pACH
 */
class BlockContentAccessHandlerTest extends CoreBlockContentAccessHandlerTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'block_content',
    'pach',
    'system',
    'user',
  ];

}
