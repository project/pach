<?php

namespace Drupal\Tests\pach\Kernel\Core\comment;

use Drupal\Tests\comment\Kernel\CommentFieldAccessTest as CoreCommentFieldAccessTest;

/**
 * Tests comment field level access.
 *
 * @group comment
 * @group Access
 * @group pACH
 */
class CommentFieldAccessTest extends CoreCommentFieldAccessTest {

  /**
   * Modules to install.
   *
   * @var array<string>
   */
  protected static $modules = [
    'comment',
    'entity_test',
    'pach',
    'user',
  ];

}
