<?php

namespace Drupal\Tests\pach\Kernel\Core\workspaces;

use Drupal\Tests\workspaces\Kernel\WorkspaceAccessTest as CoreWorkspaceAccessTest;

/**
 * Tests access on workspaces.
 *
 * @group workspaces
 * @group pACH
 */
class WorkspaceAccessTest extends CoreWorkspaceAccessTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pach',
    'path_alias',
    'user',
    'system',
    'workspaces',
    'workspace_access_test',
  ];

}
