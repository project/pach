<?php

namespace Drupal\Tests\pach\Kernel\content_moderation;

use Drupal\Tests\content_moderation\Kernel\ContentModerationAccessTest as CoreContentModerationAccessTest;

/**
 * Tests content moderation access.
 *
 * @group content_moderation
 */
class ContentModerationAccessTest extends CoreContentModerationAccessTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'filter',
    'node',
    'pach',
    'system',
    'user',
    'workflows',
  ];

}
