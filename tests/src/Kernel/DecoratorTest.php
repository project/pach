<?php

namespace Drupal\Tests\pach\KernelTests;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test decorating Drupal\pach\Entity\EntityTypeManager.
 */
class DecoratorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['pach'];

  /**
   * Tests decorating the EntityTypeManager.
   */
  public function testDecoratedEntityTypeManager(): void {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = \Drupal::service('entity_type.manager');
    // After installing "pach", the "entity_type.manager" service should return
    // the  class provided by "pach".
    $this->assertEquals('Drupal\pach\Entity\EntityTypeManager', $entity_type_manager::class);
  }

}
