<?php

namespace Drupal\Tests\pach\Functional\Core\taxonomy;

use Drupal\Tests\taxonomy\Functional\TermAccessTest as CoreTermAccessTest;

/**
 * Tests the taxonomy term access permissions.
 *
 * @group taxonomy
 * @group pACH
 */
class TermAccessTest extends CoreTermAccessTest {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'block',
    'taxonomy',
    'pach',
  ];

}
