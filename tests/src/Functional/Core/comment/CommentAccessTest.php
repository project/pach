<?php

namespace Drupal\Tests\pach\Functional\Core\comment;

use Drupal\Tests\comment\Functional\CommentAccessTest as CoreCommentAccessTest;

/**
 * Tests comment administration and preview access.
 *
 * @group comment
 * @group pACH
 */
class CommentAccessTest extends CoreCommentAccessTest {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'node',
    'comment',
    'pach',
  ];

}
