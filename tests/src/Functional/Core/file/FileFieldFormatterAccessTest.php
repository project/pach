<?php

namespace Drupal\Tests\pach\Functional\file;

use Drupal\Tests\file\Functional\FileFieldFormatterAccessTest as CoreFileFieldFormatterAccessTest;

/**
 * Tests file formatter access.
 *
 * @group file
 * @group pACH
 */
class FileFieldFormatterAccessTest extends CoreFileFieldFormatterAccessTest {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'field_ui',
    'file',
    'file_test',
    'node',
    'pach',
  ];

}
