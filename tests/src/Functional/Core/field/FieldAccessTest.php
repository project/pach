<?php

namespace Drupal\Tests\pach\Functional\field;

use Drupal\Tests\field\Functional\FieldAccessTest as CoreFieldAccessTest;

/**
 * Tests Field access.
 *
 * @group field
 * @group pACH
 */
class FieldAccessTest extends CoreFieldAccessTest {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = [
    'node',
    'field_test',
    'pach',
  ];

}
