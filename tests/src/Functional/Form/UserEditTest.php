<?php

namespace Drupal\Tests\pach\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests user edit page with special access restrictions.
 *
 * @group user
 * @group pACH
 */
class UserEditTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['pach', 'pach_extend_test'];

  /**
   * Tests the account form implements entity field access for mail.
   */
  public function testUserMailFieldAccess(): void {
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);
    $this->drupalGet('user/' . $user->id() . '/edit');
    // Mail field must not exist as it is hidden in
    // \Drupal\pach_extend_test\UserAccessControlHandler.
    $this->assertFalse($this->getSession()->getPage()->hasField('mail'));

    // Assert custom message exist in form.
    $this->assertTrue($this->getSession()->getPage()->hasContent('You are not allowed to update your email address.'));
  }

}
