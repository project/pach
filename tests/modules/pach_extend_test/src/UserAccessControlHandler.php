<?php

namespace Drupal\pach_extend_test;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserAccessControlHandler as CoreUserAccessControlHandler;

/**
 * Defines an extended access control handler for the user entity type.
 *
 * @see \Drupal\user\Entity\User
 * @see \Drupal\user\UserAccessControlHandler
 */
class UserAccessControlHandler extends CoreUserAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    $return = parent::checkFieldAccess($operation, $field_definition, $account, $items);
    $field_name = $field_definition->getName();

    // Do not allow editing the email address for all users without permission
    // to administer users.
    if (($field_name === 'mail') && ($operation == 'edit') && !$this->allowEditMailField($account)) {
      $return = AccessResult::forbidden()->cachePerUser();
    }

    return $return;
  }

  /**
   * Helper function to check if editing the email field is allowed.
   *
   * Used to test calling a static method that does not exists in other access
   * control handlers.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   *
   * @return bool
   *   <code>TRUE</code> if editing is allowed, otherwise <code>FALSE</code>.
   */
  public static function allowEditMailFieldStatic(AccountInterface $account): bool {
    return $account->hasPermission('administer users');
  }

  /**
   * Helper function to check if editing the email field is allowed.
   *
   * Used to test calling a method that does not exists in other access control
   * handlers.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account.
   *
   * @return bool
   *   <code>TRUE</code> if editing is allowed, otherwise <code>FALSE</code>.
   */
  public function allowEditMailField(AccountInterface $account): bool {
    return $account->hasPermission('administer users');
  }

}
