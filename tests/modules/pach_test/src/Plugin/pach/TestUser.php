<?php

namespace Drupal\pach_test\Plugin\pach;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\pach\Attribute\AccessControlHandler;
use Drupal\pach\Plugin\AccessControlHandlerBase;

/**
 * Test access control handler plugin for user accounts.
 */
#[AccessControlHandler(
  id: 'pach_test_user',
  type: 'user',
  weight: -10
)]
class TestUser extends AccessControlHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function applies(EntityInterface $entity, string $operation, AccountInterface $account = NULL): bool {
    /** @var \Drupal\Core\Session\AccountInterface $entity */
    return $entity->isAuthenticated();
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccessResultInterface &$access, EntityInterface $entity, string $operation, AccountInterface $account = NULL): void {
    /** @var string $email */
    $email = ($account instanceof AccountInterface) ? $account->getEmail() ?? '' : '';
    if ($operation === 'view label' && strpos($email, '@example.org')) {
      $access = $access->andIf(AccessResult::forbidden());
    }
  }

}
