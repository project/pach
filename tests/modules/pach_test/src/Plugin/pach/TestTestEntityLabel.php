<?php

namespace Drupal\pach_test\Plugin\pach;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\pach\Attribute\AccessControlHandler;
use Drupal\pach\Plugin\AccessControlHandlerBase;

/**
 * Test access control handler plugin for entities of type "entity_test_label".
 */
#[AccessControlHandler(
  id: 'pach_test_test_entity_label',
  type: 'entity_test_label',
  weight: -10
)]
class TestTestEntityLabel extends AccessControlHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function applies(EntityInterface $entity, string $operation, AccountInterface $account = NULL): bool {
    // Applies to all entities of this type.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccessResultInterface &$access, EntityInterface $entity, string $operation, AccountInterface $account = NULL): void {
    /** @var \Drupal\entity_test\Entity\EntityTestLabel $entity */
    /** @var string $label */
    $label = $entity->label();
    if (($operation === 'view label') && (stripos($label, 'no view') !== FALSE)) {
      // Deny access to all entity_test_label entities having the string
      // "no view" in the label.
      $access = $access->andIf(AccessResult::forbidden());
    }
  }

}
